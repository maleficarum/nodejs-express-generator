# generator-nodejs-express [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]
> Yoeman generator to create a NodeJS express template application

## Installation

First, install [Yeoman](http://yeoman.io) and generator-nodejs-express using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-nodejs-express
```

Then generate your new project:

```bash
yo nodejs-express
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

Apache-2.0 © [Maleficarum](maleficarum.mx)


[npm-image]: https://badge.fury.io/js/generator-nodejs-express.svg
[npm-url]: https://npmjs.org/package/generator-nodejs-express
[travis-image]: https://travis-ci.com/maleficarum/generator-nodejs-express.svg?branch=master
[travis-url]: https://travis-ci.com/maleficarum/generator-nodejs-express
[daviddm-image]: https://david-dm.org/maleficarum/generator-nodejs-express.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/maleficarum/generator-nodejs-express
[coveralls-image]: https://coveralls.io/repos/maleficarum/generator-nodejs-express/badge.svg
[coveralls-url]: https://coveralls.io/r/maleficarum/generator-nodejs-express
