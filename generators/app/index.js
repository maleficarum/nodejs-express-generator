'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');

module.exports = class extends Generator {
  prompting() {
    // Have Yeoman greet the user.
    this.log(
      yosay(
        `Welcome to the peachy ${chalk.red('generator-nodejs-express')} generator!`
      )
    );

    const prompts = [
      {
        type: "input",
        name: "projectName",
        message: "Project Name",
        default: "MyAwesomeProject"
      },
      {
        type: "input",
        name: "projectDesc",
        message: "Project description",
        default: "This is my awesome project"
      },
      {
        type: "input",
        name: "projectAuthor",
        message: "Project author",
        default: "nobody"
      },
      {
        type: 'input',
        name: 'httpPort',
        message: 'Listening port for HTTP server (default 8080)?',
        default: 8080
      }
    ];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
    });
  }

  writing() {
    this.fs.copyTpl(
      this.templatePath('lib/common.js'),
      this.destinationPath('lib/common.js'),
      this.props
    );          

    this.fs.copyTpl(
      this.templatePath('config/config.json'),
      this.destinationPath('config/config.json'),
      this.props
    );        

    this.fs.copyTpl(
      this.templatePath('lib/http.js'),
      this.destinationPath('lib/http.js'),
      this.props
    );  

    this.fs.copyTpl(
      this.templatePath('index.js'),
      this.destinationPath('index.js'),
      this.props
    );  

    this.fs.copyTpl(
      this.templatePath('package.json'),
      this.destinationPath('package.json'),
      this.props
    ); 


    this.fs.copyTpl(
      this.templatePath('test/test.js'),
      this.destinationPath('test/test.js'),
      this.props
    );     

    this.fs.copyTpl(
      this.templatePath('views/index.handlebars'),
      this.destinationPath('views/index.handlebars'),
      this.props
    );
    
    this.fs.copyTpl(
      this.templatePath('views/layouts/main.handlebars'),
      this.destinationPath('views/layouts/main.handlebars'),
      this.props
    );

    this.fs.copyTpl(
      this.templatePath('public/README.txt'),
      this.destinationPath('public/README.txt'),
      this.props
    );

    this.log(
      yosay(chalk.bold(`Remember to update your ${chalk.yellow("config/config.json")} file with the proper configuration for your application.`))
    )
  }

  install() {
    this.installDependencies({ bower: false });
  }

  end() {
    this.log(
      yosay(chalk.bold(`Let's start our new server running ${chalk.green("node index.js  | bunyan -L")}`))
    );
  }  
};
