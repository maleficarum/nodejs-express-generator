"use strict";

const http = require("./lib/http.js");
const config = require('./config/config.json')

if(process.env.HTTP_PORT) {
    config.http.port = process.env.HTTP_PORT;
}

http.config(config);
http.start();