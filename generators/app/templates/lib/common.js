"use strict";

const bunyan = require("bunyan");

const self = module.exports = {
    getLogger : (moduleName) => {
        return bunyan.createLogger({
            name: moduleName,
            src: false,
            streams: [
                {
                    level:'trace',
                    stream: process.stdout
                },
                {
                    level: 'debug',
                    path: '/tmp/' + moduleName + ".log"
                },
                {
                    level: 'error',
                    path: '/tmp/' + moduleName + ".err"
                }
              ]
        });
    }
};