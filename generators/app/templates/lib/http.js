"use strict";

const helmet = require('helmet');
const express = require('express');
const exphbs  = require('express-handlebars');
const bodyParser = require('body-parser');
const got = require('got');
const common = require('./common.js');
const cookieParser = require("cookie-parser");
const session = require('express-session');

const MODULE_NAME = "http-server";
const app = express();

var config;
var log;

const self = module.exports = {
    config: (__config__) => {
        config = __config__;

        log = common.getLogger(MODULE_NAME);

        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));
        app.disable('x-powered-by');
        app.use(helmet());
        app.use(cookieParser());

        app.use(session({
            key: 'maleficarum_id',
            secret: 'M@l3f1C@ruM',
            resave: false,
            saveUninitialized: false,
            cookie: {
                expires: 1800000
            }
        }));


        app.engine('handlebars', exphbs({
            defaultLayout: 'main',
            layoutsDir: 'views/layouts',
            partialsDir: 'views/partials',
            helpers:{
              endOfList:(a, b) => {
                return a == b ? "" : ",";
              },
              dateFormat: require('handlebars-dateformat')
            }
        }));
      
        app.set('view engine', 'handlebars');
        app.disable('etag').disable('x-powered-by');
        app.use(express.static('public'));        
        app.set('views', 'views');

    },
    start: () => {
        log.info("Listening on port ", config.http.port);

        self.endpoints();

        app.listen(config.http.port, () => {
            log.info('Listening on port ' + config.http.port);
        });

        log.debug("Try sending a request : curl -X POST -H 'Content-type: application/json' -d '{\"name\":\"Client\"}' http://localhost:8080/hello")
    },
    endpoints: () => {

        /*
        * Handler to clear cookies if the userid or username isn't set. 
        * Customize the properties you want to handle in the cookies
        */

        app.use((req, res, next) => {
            if (req.cookies.user_id && !req.session.user) {
                res.clearCookie('maleficarum_id');
            }
            next();
        });

        /**
         * Web application entry point
         */
        app.get('/', (req, res) => {
            res.render('index', {layout: false});
        });

        /**
         * POST entpoint to test the server
         */
        app.post('/hello', (req, res) => {
            res.send({
                "message": "Hi " + req.body.name
            });
        });

        /**
         * First page after perform your login procedure
         * The sessionChecker middleware validates if the session and cookie is already set using your login function, if not, redirects to /.
         */
        app.get('/home', self.sessionChecker, (req, res) => {
            res.render('home');
        });
    },
    sessionChecker : (req, res, next) => {
        if (req.session.user == undefined || req.session.client == undefined || req.cookies.maleficarum_id == undefined) {
            res.redirect('/');
        } else {
            next();
        }
    }
};